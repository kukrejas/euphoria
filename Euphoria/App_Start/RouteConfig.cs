﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Euphoria.Services;

namespace Euphoria
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Home",
                url: "Home",
                defaults: new { controller = "Home", action = "Home", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Apply",
                url: "Apply",
                defaults: new { controller = "Home", action = "Apply", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "User",
                url: "{username}",
                defaults: new { controller = "User", action = "Get" },
                constraints: new { username = new UserNameConstraint() }
            );

            routes.MapRoute(
                name: "AllNews",
                url: "News",
                defaults: new { controller = "Home", action = "Home" }
            );

            routes.MapRoute(
                name: "AddComment",
                url: "News/AddComment",
                defaults: new { controller = "News", action = "AddComment"}
            );

            routes.MapRoute(
                name: "Applications",
                url: "Applications",
                defaults: new { controller = "Home", action = "Applications" }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Default", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "News",
                url: "News/{id}",
                defaults: new { controller = "News", action = "View", id = UrlParameter.Optional }
            );
        }

        public class UserNameConstraint : IRouteConstraint
        {
            public bool Match(HttpContextBase httpContext, Route route, string parameterName, RouteValueDictionary values, RouteDirection routeDirection)
            {
                List<string> users = CacheService.GetUsernameList();
                var username = values["username"].ToString().ToLower();
                // Check for a match (assumes case insensitive)
                return users.Any(x => x.ToLower() == username);
            }
        }
    }
}
