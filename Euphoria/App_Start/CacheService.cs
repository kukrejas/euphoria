﻿// Decompiled with JetBrains decompiler
// Type: Euphoria.Services.CacheService
// Assembly: Euphoria, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 495B1798-C730-4936-A44B-A3E925684694
// Assembly location: E:\Development\Backup\bin\Euphoria.dll

using Euphoria.Context;
using RestSharp;
using RestSharp.Deserializers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.Caching;

namespace Euphoria.Services
{
    public class CacheService
    {
        private static object lck = new object();

        public static List<string> GetUsernameList()
        {
            string key = "usernames";
            List<string> stringList = MemoryCache.Default.Get(key, (string)null) as List<string>;
            if (stringList == null)
            {
                lock (CacheService.lck)
                {
                    stringList = MemoryCache.Default.Get(key, (string)null) as List<string>;
                    if (stringList != null)
                        return stringList;
                    stringList = CacheService.UsernamesFromDB();
                    DateTimeOffset absoluteExpiration = DateTimeOffset.UtcNow.AddMinutes(1440.0);
                    MemoryCache.Default.AddOrGetExisting(key, (object)stringList, absoluteExpiration, (string)null);
                }
            }
            return stringList;
        }

        public static List<FriendStatus> GetAllFriends()
        {
            string key = "allfriends";
            List<FriendStatus> friendStatusList = MemoryCache.Default.Get(key, (string)null) as List<FriendStatus>;
            if (friendStatusList == null)
            {
                lock (CacheService.lck)
                {
                    friendStatusList = MemoryCache.Default.Get(key, (string)null) as List<FriendStatus>;
                    if (friendStatusList != null)
                        return friendStatusList;
                    friendStatusList = CacheService.AllFriendsFromDB();
                    DateTimeOffset absoluteExpiration = DateTimeOffset.UtcNow.AddMinutes(1440.0);
                    MemoryCache.Default.AddOrGetExisting(key, (object)friendStatusList, absoluteExpiration, (string)null);
                }
            }
            return friendStatusList;
        }

        public static List<Achievement> GetAchievementList()
        {
            string key = "achievements";
            List<Achievement> achievementList = MemoryCache.Default.Get(key, (string)null) as List<Achievement>;
            if (achievementList == null)
            {
                lock (CacheService.lck)
                {
                    achievementList = MemoryCache.Default.Get(key, (string)null) as List<Achievement>;
                    if (achievementList != null)
                        return achievementList;
                    achievementList = CacheService.AchievementListFromDB();
                    DateTimeOffset absoluteExpiration = DateTimeOffset.UtcNow.AddMinutes(1440.0);
                    MemoryCache.Default.AddOrGetExisting(key, (object)achievementList, absoluteExpiration, (string)null);
                }
            }
            return achievementList;
        }

        public static List<User> GetUserList()
        {
            string key = "users";
            List<User> userList = MemoryCache.Default.Get(key, (string)null) as List<User>;
            if (userList == null)
            {
                lock (CacheService.lck)
                {
                    userList = MemoryCache.Default.Get(key, (string)null) as List<User>;
                    if (userList != null)
                        return userList;
                    userList = CacheService.UsersFromDB();
                    DateTimeOffset absoluteExpiration = DateTimeOffset.UtcNow.AddMinutes(1440.0);
                    MemoryCache.Default.AddOrGetExisting(key, (object)userList, absoluteExpiration, (string)null);
                }
            }
            return userList;
        }

        public static List<TwitchUser> GetTwitchList()
        {
            string key = "twitch";
            List<TwitchUser> twitchUserList = MemoryCache.Default.Get(key, (string)null) as List<TwitchUser>;
            if (twitchUserList == null)
            {
                lock (CacheService.lck)
                {
                    twitchUserList = MemoryCache.Default.Get(key, (string)null) as List<TwitchUser>;
                    if (twitchUserList != null)
                        return twitchUserList;
                    twitchUserList = CacheService.TwitchFromDB();
                    DateTimeOffset absoluteExpiration = DateTimeOffset.UtcNow.AddMinutes(60.0);
                    MemoryCache.Default.AddOrGetExisting(key, (object)twitchUserList, absoluteExpiration, (string)null);
                }
            }
            return twitchUserList;
        }

        private static List<TwitchUser> TwitchFromDB()
        {
            List<string> source = new List<string>();
            List<TwitchUser> twitchUserList = new List<TwitchUser>();
            using (Connection connection = new Connection())
                source = connection.Users.Select<User, string>((Expression<Func<User, string>>)(u => u.TwitchID)).Where<string>((Expression<Func<string, bool>>)(id => id != default(string) && id != "")).ToList<string>();
            if (source.Count > 0)
            {
                string str = "?login=";
                for (int index = 0; index < source.Count; ++index)
                {
                    str += source.ElementAt<string>(index);
                    if (index < source.Count - 1)
                        str += "&login=";
                }
                Uri uri = new Uri("https://api.twitch.tv/helix/users" + str);
                RestClient restClient = new RestClient("https://api.twitch.tv/helix/");
                RestRequest restRequest = new RestRequest("users" + str, Method.GET);
                restRequest.AddHeader("Client-ID", "1gwwfmdlmxs9q6uv39q29zjhr0bvfp");
                twitchUserList = new JsonDeserializer().Deserialize<List<RootObject>>(restClient.Execute((IRestRequest)restRequest)).FirstOrDefault<RootObject>().data.Select<TwitchResponse, TwitchUser>((Func<TwitchResponse, TwitchUser>)(x => new TwitchUser()
                {
                    TwitchID = x.id,
                    TwitchName = x.login
                })).ToList<TwitchUser>();
            }
            return twitchUserList;
        }

        private static List<FriendStatus> AllFriendsFromDB()
        {
            List<FriendStatus> friendStatusList = new List<FriendStatus>();
            using (Models.Procedures procedures = new Models.Procedures())
                return procedures.GetAllFriends().Select<Models.GetAllFriends_Result, FriendStatus>((Func<Models.GetAllFriends_Result, FriendStatus>)(f => new FriendStatus()
                {
                    FriendID = f.FriendID,
                    FriendRequestID = f.FriendRequestID,
                    RequestDate = f.RequestDate,
                    UserID1 = f.UserID1,
                    UserID2 = f.UserID2
                })).ToList<FriendStatus>();
        }

        private static List<Achievement> AchievementListFromDB()
        {
            List<Achievement> achievementList = new List<Achievement>();
            using (Connection connection = new Connection())
                return connection.Achievements.Select<Achievement, Achievement>((Expression<Func<Achievement, Achievement>>)(u => u)).ToList<Achievement>();
        }

        private static List<User> UsersFromDB()
        {
            List<User> userList = new List<User>();
            using (Connection connection = new Connection())
                return connection.Users.Select<User, User>((Expression<Func<User, User>>)(u => u)).ToList<User>();
        }

        private static List<string> UsernamesFromDB()
        {
            List<string> stringList = new List<string>();
            using (Connection connection = new Connection())
                return connection.Users.Select<User, string>((Expression<Func<User, string>>)(u => u.UserName)).ToList<string>();
        }
    }

    public class RootObject
    {
        public List<TwitchResponse> data { get; set; }
    }
}
