using System;
using System.Data.Entity.Validation;
using System.Text;

namespace Euphoria.Context
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class Connection : DbContext
    {
        public Connection()
            : base("name=Connection")
        {
        }

        public virtual DbSet<Application> Applications { get; set; }
        public virtual DbSet<Game> Games { get; set; }
        public virtual DbSet<News> News { get; set; }
        public virtual DbSet<Role> Roles { get; set; }
        public virtual DbSet<Friend> Friends { get; set; }
        public virtual DbSet<FriendRequest> FriendRequests { get; set; }
        public virtual DbSet<ProfilePost> ProfilePosts { get; set; }
        public virtual DbSet<Activity> Activity { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<Comment> Comment { get; set; }
        public virtual DbSet<Achievement> Achievements { get; set; }
        public virtual DbSet<UserAchievementProgress> UserAchievementProgresses { get; set; }
        public virtual DbSet<UserAchievement> UserAchievements { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ProfilePost>()
                .Property(e => e.PostContent)
                .IsUnicode(false);

            modelBuilder.Entity<Application>()
                .Property(e => e.Game)
                .IsUnicode(false);

            modelBuilder.Entity<Application>()
                .Property(e => e.CharacterName)
                .IsUnicode(false);

            modelBuilder.Entity<Application>()
                .Property(e => e.Answers)
                .IsUnicode(false);

            modelBuilder.Entity<Game>()
                .Property(e => e.GameName)
                .IsUnicode(false);

            modelBuilder.Entity<Activity>()
                .Property(e => e.Message)
                .IsUnicode(false);

            modelBuilder.Entity<News>()
                .Property(e => e.Title)
                .IsUnicode(false);

            modelBuilder.Entity<News>()
                .Property(e => e.Content)
                .IsUnicode(false);

            modelBuilder.Entity<News>()
                .Property(e => e.Author)
                .IsUnicode(false);

            modelBuilder.Entity<Comment>()
                .Property(e => e.CommentID);

            modelBuilder.Entity<Role>()
                .HasMany(e => e.Users)
                .WithMany(e => e.Roles)
                .Map(m => m.ToTable("UserRole").MapLeftKey("RoleId").MapRightKey("UserId"));

            modelBuilder.Entity<Achievement>()
                .Property(e => e.Title)
                .IsUnicode(false);

            modelBuilder.Entity<Achievement>()
                .Property(e => e.Description)
                .IsUnicode(false);

            modelBuilder.Entity<Achievement>()
                .Property(e => e.BadgeIcon)
                .IsUnicode(false);

            modelBuilder.Entity<Achievement>()
                .HasMany(e => e.UserAchievements)
                .WithRequired(e => e.Achievement)
                .WillCascadeOnDelete(false);
        }

        public override int SaveChanges()
        {
            try
            {
                return base.SaveChanges();
            }
            catch (DbEntityValidationException e)
            {
                var newException = new FormattedDbEntityValidationException(e);
                throw newException;
            }
        }
    }
}

public class FormattedDbEntityValidationException : Exception
{
    public FormattedDbEntityValidationException(DbEntityValidationException innerException) :
        base(null, innerException)
    {
    }

    public override string Message
    {
        get
        {
            var innerException = InnerException as DbEntityValidationException;
            if (innerException != null)
            {
                StringBuilder sb = new StringBuilder();

                sb.AppendLine();
                sb.AppendLine();
                foreach (var eve in innerException.EntityValidationErrors)
                {
                    sb.AppendLine(string.Format("- Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().FullName, eve.Entry.State));
                    foreach (var ve in eve.ValidationErrors)
                    {
                        sb.AppendLine(string.Format("-- Property: \"{0}\", Value: \"{1}\", Error: \"{2}\"",
                            ve.PropertyName,
                            eve.Entry.CurrentValues.GetValue<object>(ve.PropertyName),
                            ve.ErrorMessage));
                    }
                }
                sb.AppendLine();

                return sb.ToString();
            }

            return base.Message;
        }
    }
}