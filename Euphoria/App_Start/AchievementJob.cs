﻿using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Caching;
using Euphoria.Context;
using Euphoria.Models;

namespace Euphoria.Services
{
    public abstract class AchievementJob
    {
        protected AchievementJob()
        {
            Insert();
        }

        protected abstract void AwardAchievements();

        protected abstract TimeSpan Interval { get; }

        private void Callback(string key, object value, CacheItemRemovedReason reason)
        {
            if (reason == CacheItemRemovedReason.Expired)
            {
                this.AwardAchievements();
                this.Insert();
            }
        }

        private void Insert()
        {
            HttpRuntime.Cache.Add(this.GetType().ToString(),
                this,
                null,
                Cache.NoAbsoluteExpiration,
                this.Interval,
                CacheItemPriority.Normal,
                this.Callback);
        }
    }

    public class CommenterBadge : AchievementJob
    {
        public CommenterBadge() : base() { }

        protected override void AwardAchievements()
        {
            using (var db = new Connection())
            {
                var context = GlobalHost.ConnectionManager.GetHubContext<Feed>();
                List<Achievement> achievements = CacheService.GetAchievementList();
                List<Euphoria.Context.User> users = CacheService.GetUserList();

                //Comments 1
                List<UserAchievementProgress> userList = db.UserAchievementProgresses.Where(u => u.CommentCount >= 1 &&  !db.UserAchievements.Any(a => a.UserID == u.UserID && a.AchievementID == 1)).ToList();
                foreach (var user in userList)
                {
                    UserAchievement achieved = new UserAchievement
                    {
                        UserID = user.UserID,
                        AchievementID = 1
                    };
                    db.UserAchievements.Add(achieved);
                    Euphoria.Context.User userObject = users.Where(u => u.UserId == user.UserID).FirstOrDefault();
                    Achievement achievement = achievements.Where(a => a.AchievementID == 1).FirstOrDefault();
                    string message = "<a href='/" + userObject.UserName + "'>" + userObject.UserName + "</a> achieved <span style='color: orange;'>" + achievement.Title + "</span>!";
                    context.Clients.All.addNewMessageToPage(achievement.BadgeIcon, message);
                    var feed = db.Set<Activity>();
                    feed.Add(new Activity { ImageSrc = achievement.BadgeIcon, Message = message });
                }

                //Comments 2
                userList = db.UserAchievementProgresses.Where(u => u.CommentCount >= 10 && !db.UserAchievements.Any(a => a.UserID == u.UserID && a.AchievementID == 2)).ToList();
                foreach (var user in userList)
                {
                    UserAchievement achieved = new UserAchievement
                    {
                        UserID = user.UserID,
                        AchievementID = 2
                    };
                    db.UserAchievements.Add(achieved);
                    Euphoria.Context.User userObject = users.Where(u => u.UserId == user.UserID).FirstOrDefault();
                    Achievement achievement = achievements.Where(a => a.AchievementID == 2).FirstOrDefault();
                    string message = "<a href='/" + userObject.UserName + "'>" + userObject.UserName + "</a> achieved <span style='color: orange;'>" + achievement.Title + "</span>!";
                    context.Clients.All.addNewMessageToPage(achievement.BadgeIcon, message);
                    var feed = db.Set<Activity>();
                    feed.Add(new Activity { ImageSrc = achievement.BadgeIcon, Message = message });
                }

                //Comments 3
                userList = db.UserAchievementProgresses.Where(u => u.CommentCount >= 25 && !db.UserAchievements.Any(a => a.UserID == u.UserID && a.AchievementID == 3)).ToList();
                foreach (var user in userList)
                {
                    UserAchievement achieved = new UserAchievement
                    {
                        UserID = user.UserID,
                        AchievementID = 3
                    };
                    db.UserAchievements.Add(achieved);
                    Euphoria.Context.User userObject = users.Where(u => u.UserId == user.UserID).FirstOrDefault();
                    Achievement achievement = achievements.Where(a => a.AchievementID == 3).FirstOrDefault();
                    string message = "<a href='/" + userObject.UserName + "'>" + userObject.UserName + "</a> achieved <span style='color: orange;'>" + achievement.Title + "</span>!";
                    context.Clients.All.addNewMessageToPage(achievement.BadgeIcon, message);
                    var feed = db.Set<Activity>();
                    feed.Add(new Activity { ImageSrc = achievement.BadgeIcon, Message = message });
                }

                //Comments 4
                userList = db.UserAchievementProgresses.Where(u => u.CommentCount >= 50 && !db.UserAchievements.Any(a => a.UserID == u.UserID && a.AchievementID == 4)).ToList();
                foreach (var user in userList)
                {
                    UserAchievement achieved = new UserAchievement
                    {
                        UserID = user.UserID,
                        AchievementID = 4
                    };
                    db.UserAchievements.Add(achieved);
                    Euphoria.Context.User userObject = users.Where(u => u.UserId == user.UserID).FirstOrDefault();
                    Achievement achievement = achievements.Where(a => a.AchievementID == 4).FirstOrDefault();
                    string message = "<a href='/" + userObject.UserName + "'>" + userObject.UserName + "</a> achieved <span style='color: orange;'>" + achievement.Title + "</span>!";
                    context.Clients.All.addNewMessageToPage(achievement.BadgeIcon, message);
                    var feed = db.Set<Activity>();
                    feed.Add(new Activity { ImageSrc = achievement.BadgeIcon, Message = message });
                }

                //Comments 5
                userList = db.UserAchievementProgresses.Where(u => u.CommentCount >= 100 && !db.UserAchievements.Any(a => a.UserID == u.UserID && a.AchievementID == 5)).ToList();
                foreach (var user in userList)
                {
                    UserAchievement achieved = new UserAchievement
                    {
                        UserID = user.UserID,
                        AchievementID = 5
                    };
                    db.UserAchievements.Add(achieved);
                    Euphoria.Context.User userObject = users.Where(u => u.UserId == user.UserID).FirstOrDefault();
                    Achievement achievement = achievements.Where(a => a.AchievementID == 5).FirstOrDefault();
                    string message = "<a href='/" + userObject.UserName + "'>" + userObject.UserName + "</a> achieved <span style='color: orange;'>" + achievement.Title + "</span>!";
                    context.Clients.All.addNewMessageToPage(achievement.BadgeIcon, message);
                    var feed = db.Set<Activity>();
                    feed.Add(new Activity { ImageSrc = achievement.BadgeIcon, Message = message });
                }
                db.SaveChanges();
            }
        }

        //run every 10 minutes
        protected override TimeSpan Interval
        {
            get { return new TimeSpan(0, 10, 0); }
        }
    }

    public class WallPostBadge : AchievementJob
    {
        public WallPostBadge() : base() { }

        protected override void AwardAchievements()
        {
            using (var db = new Connection())
            {
                var context = GlobalHost.ConnectionManager.GetHubContext<Feed>();
                List<Achievement> achievements = CacheService.GetAchievementList();
                List<Euphoria.Context.User> users = CacheService.GetUserList();

                //Wall Posts 1
                List<UserAchievementProgress> userList = db.UserAchievementProgresses.Where(u => u.WallPostCount >= 1 && !db.UserAchievements.Any(a => a.UserID == u.UserID && a.AchievementID == 6)).ToList();
                foreach (var user in userList)
                {
                    UserAchievement achieved = new UserAchievement
                    {
                        UserID = user.UserID,
                        AchievementID = 6
                    };
                    db.UserAchievements.Add(achieved);
                    Euphoria.Context.User userObject = users.Where(u => u.UserId == user.UserID).FirstOrDefault();
                    Achievement achievement = achievements.Where(a => a.AchievementID == 6).FirstOrDefault();
                    string message = "<a href='/" + userObject.UserName + "'>" + userObject.UserName + "</a> achieved <span style='color: orange;'>" + achievement.Title + "</span>!";
                    context.Clients.All.addNewMessageToPage(achievement.BadgeIcon, message);
                    var feed = db.Set<Activity>();
                    feed.Add(new Activity { ImageSrc = achievement.BadgeIcon, Message = message });
                }

                //Wall Posts 2
                userList = db.UserAchievementProgresses.Where(u => u.WallPostCount >= 10 && !db.UserAchievements.Any(a => a.UserID == u.UserID && a.AchievementID == 7)).ToList();
                foreach (var user in userList)
                {
                    UserAchievement achieved = new UserAchievement
                    {
                        UserID = user.UserID,
                        AchievementID = 7
                    };
                    db.UserAchievements.Add(achieved);
                    Euphoria.Context.User userObject = users.Where(u => u.UserId == user.UserID).FirstOrDefault();
                    Achievement achievement = achievements.Where(a => a.AchievementID == 7).FirstOrDefault();
                    string message = "<a href='/" + userObject.UserName + "'>" + userObject.UserName + "</a> achieved <span style='color: orange;'>" + achievement.Title + "</span>!";
                    context.Clients.All.addNewMessageToPage(achievement.BadgeIcon, message);
                    var feed = db.Set<Activity>();
                    feed.Add(new Activity { ImageSrc = achievement.BadgeIcon, Message = message });
                }

                //Wall Posts 3
                userList = db.UserAchievementProgresses.Where(u => u.WallPostCount >= 25 && !db.UserAchievements.Any(a => a.UserID == u.UserID && a.AchievementID == 8)).ToList();
                foreach (var user in userList)
                {
                    UserAchievement achieved = new UserAchievement
                    {
                        UserID = user.UserID,
                        AchievementID = 8
                    };
                    db.UserAchievements.Add(achieved);
                    Euphoria.Context.User userObject = users.Where(u => u.UserId == user.UserID).FirstOrDefault();
                    Achievement achievement = achievements.Where(a => a.AchievementID == 8).FirstOrDefault();
                    string message = "<a href='/" + userObject.UserName + "'>" + userObject.UserName + "</a> achieved <span style='color: orange;'>" + achievement.Title + "</span>!";
                    context.Clients.All.addNewMessageToPage(achievement.BadgeIcon, message);
                    var feed = db.Set<Activity>();
                    feed.Add(new Activity { ImageSrc = achievement.BadgeIcon, Message = message });
                }

                //Wall Posts 4
                userList = db.UserAchievementProgresses.Where(u => u.WallPostCount >= 50 && !db.UserAchievements.Any(a => a.UserID == u.UserID && a.AchievementID == 9)).ToList();
                foreach (var user in userList)
                {
                    UserAchievement achieved = new UserAchievement
                    {
                        UserID = user.UserID,
                        AchievementID = 9
                    };
                    db.UserAchievements.Add(achieved);
                    Euphoria.Context.User userObject = users.Where(u => u.UserId == user.UserID).FirstOrDefault();
                    Achievement achievement = achievements.Where(a => a.AchievementID == 9).FirstOrDefault();
                    string message = "<a href='/" + userObject.UserName + "'>" + userObject.UserName + "</a> achieved <span style='color: orange;'>" + achievement.Title + "</span>!";
                    context.Clients.All.addNewMessageToPage(achievement.BadgeIcon, message);
                    var feed = db.Set<Activity>();
                    feed.Add(new Activity { ImageSrc = achievement.BadgeIcon, Message = message });
                }

                //Wall Posts 5
                userList = db.UserAchievementProgresses.Where(u => u.WallPostCount >= 100 && !db.UserAchievements.Any(a => a.UserID == u.UserID && a.AchievementID == 10)).ToList();
                foreach (var user in userList)
                {
                    UserAchievement achieved = new UserAchievement
                    {
                        UserID = user.UserID,
                        AchievementID = 10
                    };
                    db.UserAchievements.Add(achieved);
                    Euphoria.Context.User userObject = users.Where(u => u.UserId == user.UserID).FirstOrDefault();
                    Achievement achievement = achievements.Where(a => a.AchievementID == 10).FirstOrDefault();
                    string message = "<a href='/" + userObject.UserName + "'>" + userObject.UserName + "</a> achieved <span style='color: orange;'>" + achievement.Title + "</span>!";
                    context.Clients.All.addNewMessageToPage(achievement.BadgeIcon, message);
                    var feed = db.Set<Activity>();
                    feed.Add(new Activity { ImageSrc = achievement.BadgeIcon, Message = message });
                }
                db.SaveChanges();
            }
        }

        //run every 10 minutes
        protected override TimeSpan Interval
        {
            get { return new TimeSpan(0, 10, 0); }
        }
    }
}