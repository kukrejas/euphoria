﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Mvc;
using Euphoria.Services;
using Microsoft.Owin.Security;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Web;
using Euphoria.Context;

/// <summary>
/// Summary description for Models
/// </summary>
namespace Euphoria.Models
{
    public class ApplicationDbContext : IdentityDbContext<User, Role, string, UserLogin, UserRole, UserClaim>
    {
        public ApplicationDbContext() : base("EuphoriaDB") { }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        protected override void OnModelCreating(System.Data.Entity.DbModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("dbo");

            modelBuilder.Entity<UserLogin>().Map(c =>
            {
                c.ToTable("UserLogin");
                c.Properties(p => new
                {
                    p.UserId,
                    p.LoginProvider,
                    p.ProviderKey
                });
            }).HasKey(p => new { p.LoginProvider, p.ProviderKey, p.UserId });

            // Mapping for ApiRole
            modelBuilder.Entity<Role>().Map(c =>
            {
                c.ToTable("Role");
                c.Property(p => p.Id).HasColumnName("RoleId");
                c.Properties(p => new
                {
                    p.Name
                });
            }).HasKey(p => p.Id);
            modelBuilder.Entity<Role>().HasMany(c => c.Users).WithRequired().HasForeignKey(c => c.RoleId);

            modelBuilder.Entity<User>().Map(c =>
            {
                c.ToTable("User");
                c.Property(p => p.Id).HasColumnName("UserId");
                c.Properties(p => new
                {
                    p.AccessFailedCount,
                    p.Email,
                    p.EmailConfirmed,
                    p.PasswordHash,
                    p.PhoneNumber,
                    p.PhoneNumberConfirmed,
                    p.TwoFactorEnabled,
                    p.SecurityStamp,
                    p.LockoutEnabled,
                    p.LockoutEndDateUtc,
                    p.UserName
                });
            }).HasKey(c => c.Id);
            modelBuilder.Entity<User>().HasMany(c => c.Logins).WithOptional().HasForeignKey(c => c.UserId);
            modelBuilder.Entity<User>().HasMany(c => c.Claims).WithOptional().HasForeignKey(c => c.UserId);
            modelBuilder.Entity<User>().HasMany(c => c.Roles).WithRequired().HasForeignKey(c => c.UserId);

            modelBuilder.Entity<UserRole>().Map(c =>
            {
                c.ToTable("UserRole");
                c.Properties(p => new
                {
                    p.UserId,
                    p.RoleId
                });
            })
            .HasKey(c => new { c.UserId, c.RoleId });

            modelBuilder.Entity<UserClaim>().Map(c =>
            {
                c.ToTable("UserClaim");
                c.Property(p => p.Id).HasColumnName("UserClaimId");
                c.Properties(p => new
                {
                    p.UserId,
                    p.ClaimValue,
                    p.ClaimType
                });
            }).HasKey(c => c.Id);
        }
    }

    public class Role : IdentityRole<string, UserRole> { }

    public class User : IdentityUser<string, UserLogin, UserRole, UserClaim>
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(ApplicationUserManager manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }
    public class UserRole : IdentityUserRole { }

    public class UserClaim : IdentityUserClaim { }

    public class UserLogin : IdentityUserLogin { }

    public class ApplicationUserStore :
    UserStore<User, Role, string, UserLogin, UserRole, UserClaim>,
    IUserStore<User>,
    IDisposable
    {
        public ApplicationUserStore(ApplicationDbContext context) : base(context) { }
    }

    public class ApplicationUserManager : UserManager<User>
    {
        public ApplicationUserManager(IUserStore<User> store)
            : base(store)
        {
        }

        public static ApplicationUserManager Create(IdentityFactoryOptions<ApplicationUserManager> options, IOwinContext context)
        {
            var manager = new ApplicationUserManager(new ApplicationUserStore(context.Get<ApplicationDbContext>()));
            // Configure validation logic for usernames
            manager.UserValidator = new UserValidator<User>(manager)
            {
                AllowOnlyAlphanumericUserNames = false,
                RequireUniqueEmail = true
            };

            // Configure validation logic for passwords
            manager.PasswordValidator = new PasswordValidator
            {
                RequiredLength = 6,
                RequireNonLetterOrDigit = true,
                RequireDigit = true,
                RequireLowercase = true,
                RequireUppercase = true,
            };

            // Configure user lockout defaults
            manager.UserLockoutEnabledByDefault = true;
            manager.DefaultAccountLockoutTimeSpan = TimeSpan.FromMinutes(5);
            manager.MaxFailedAccessAttemptsBeforeLockout = 5;

            // Register two factor authentication providers. This application uses Phone and Emails as a step of receiving a code for verifying the user
            // You can write your own provider and plug it in here.
            manager.RegisterTwoFactorProvider("Phone Code", new PhoneNumberTokenProvider<User>
            {
                MessageFormat = "Your security code is {0}"
            });
            manager.RegisterTwoFactorProvider("Email Code", new EmailTokenProvider<User>
            {
                Subject = "Security Code",
                BodyFormat = "Your security code is {0}"
            });
            manager.EmailService = new EmailService();

            var dataProtectionProvider = options.DataProtectionProvider;
            if (dataProtectionProvider != null)
            {
                manager.UserTokenProvider =
                    new DataProtectorTokenProvider<User>(dataProtectionProvider.Create("ASP.NET Identity"));
            }
            return manager;
        }
    }

    public class ApplicationSignInManager : SignInManager<User, string>
    {
        public ApplicationSignInManager(ApplicationUserManager userManager, IAuthenticationManager authenticationManager)
            : base(userManager, authenticationManager)
        {
        }

        public override Task<ClaimsIdentity> CreateUserIdentityAsync(User user)
        {
            return user.GenerateUserIdentityAsync((ApplicationUserManager)UserManager);
        }

        public static ApplicationSignInManager Create(IdentityFactoryOptions<ApplicationSignInManager> options, IOwinContext context)
        {
            return new ApplicationSignInManager(context.GetUserManager<ApplicationUserManager>(), context.Authentication);
        }
    }

    public class IndexViewModel
    {
        public bool HasPassword { get; set; }
        public IList<UserLoginInfo> Logins { get; set; }
        public string PhoneNumber { get; set; }
        public bool TwoFactor { get; set; }
        public bool BrowserRemembered { get; set; }
    }

    public class ManageLoginsViewModel
    {
        public IList<UserLoginInfo> CurrentLogins { get; set; }
        public IList<AuthenticationDescription> OtherLogins { get; set; }
    }

    public class FactorViewModel
    {
        public string Purpose { get; set; }
    }

    public class SetPasswordViewModel
    {
        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
        [System.ComponentModel.DataAnnotations.Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }

    public class ChangePasswordViewModel
    {
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Current password")]
        public string OldPassword { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
        [System.ComponentModel.DataAnnotations.Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }

    public class AddPhoneNumberViewModel
    {
        [Required]
        [Phone]
        [Display(Name = "Phone Number")]
        public string Number { get; set; }
    }

    public class VerifyPhoneNumberViewModel
    {
        [Required]
        [Display(Name = "Code")]
        public string Code { get; set; }

        [Required]
        [Phone]
        [Display(Name = "Phone Number")]
        public string PhoneNumber { get; set; }
    }

    public class ConfigureTwoFactorViewModel
    {
        public string SelectedProvider { get; set; }
        public ICollection<System.Web.Mvc.SelectListItem> Providers { get; set; }
    }

    public class ExternalLoginConfirmationViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class ExternalLoginListViewModel
    {
        public string ReturnUrl { get; set; }
    }

    public class SendCodeViewModel
    {
        public string SelectedProvider { get; set; }
        public ICollection<System.Web.Mvc.SelectListItem> Providers { get; set; }
        public string ReturnUrl { get; set; }
        public bool RememberMe { get; set; }
    }

    public class VerifyCodeViewModel
    {
        [Required]
        public string Provider { get; set; }

        [Required]
        [Display(Name = "Code")]
        public string Code { get; set; }
        public string ReturnUrl { get; set; }

        [Display(Name = "Remember this browser?")]
        public bool RememberBrowser { get; set; }

        public bool RememberMe { get; set; }
    }

    public class ForgotViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class LoginViewModel
    {
        [Required]
        [Display(Name = "Username")]
        public string Username { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }
    }

    public class RegisterViewModel
    {
        [Required]
        [Display(Name = "Username")]
        public string Username { get; set; }

        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }

    public class ApplicationViewModel
    {
        [Required]
        [Display(Name = "What is your character name?")]
        public string CharacterName { get; set; }

        [Required]
        [Display(Name = "What is your primary character class?")]
        public string Class { get; set; }

        [Required]
        [Display(Name = "Where are you from (please provide a timezone)?")]
        public string Location { get; set; }

        [Required]
        [Display(Name = "What is your age?")]
        public string Age { get; set; }

        [Required]
        [Display(Name = "How much experience do you have playing MapleStory 2?")]
        public string Experience { get; set; }

        [Required]
        [Display(Name = "Which MapleStory 2 guild(s) were you in previously? (if any)")]
        public string PrevGuilds { get; set; }

        [Required]
        [Display(Name = "Do you have any experiences with guilds/clans in other MMORPGs? If yes, please specify.")]
        public string PrevMMOs { get; set; }

        [Required]
        [Display(Name = "If you were accepted into Euphoria, would you be willing to contribute to the guild? How?")]
        public string Contribute { get; set; }

        [Required]
        [Display(Name = "Are you interested in PvP, PvE or both?")]
        public string Preference { get; set; }

        [Required]
        [Display(Name = "Are you comfortable using a microphone on voice communication programs such as Discord?")]
        public string Voice { get; set; }

        [Required]
        [Display(Name = "Do you currently play any games other than MapleStory 2? (please specify)")]
        public string OtherGames { get; set; }

        [Required]
        [Display(Name = "Which of these describes you best?")]
        public string PlayerType { get; set; }

        [Required]
        [Display(Name = "Why do you want to join Euphoria?")]
        public string LookingFor { get; set; }

        [Required]
        [Display(Name = "Please enter your Discord username so we can contact you: ")]
        public string DiscordHandle { get; set; }

        [Display(Name = "Please enter your Twitch handle (optional): ")]
        public string TwitchHandle { get; set; }

        [Display(Name = "Please enter your YouTube channel (optional): ")]
        public string YouTubeHandle { get; set; }
    }

    public class ResetPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        public string Code { get; set; }
    }

    public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class NewsViewModel
    {
        [Required]
        [AllowHtml]
        [Display(Name = "Content")]
        public string Content { get; set; }

        [Required]
        [Display(Name = "Article Title")]
        public string Title { get; set; }

        [Required, Microsoft.Web.Mvc.FileExtensions(Extensions = "jpg,jpeg,png",
             ErrorMessage = "Only .jpg and .png files are accepted.")]
        [Display(Name = "Choose a file")]
        public HttpPostedFileBase FeaturedImage { get; set; }

        public string FeaturedImagePath { get; set; }

        [Display(Name = "Article ID")]
        public int ArticleID { get; set; }

        [Display(Name = "Author")]
        public string Author { get; set; }

        [Display(Name = "Category")]
        public string Category { get; set; }

        [Display(Name = "Published Date")]
        public DateTime Published { get; set; }
    }

    public class CommentsViewModel
    {
        public int CommentID { get; set; }

        public int ParentID { get; set; }

        [Required]
        [AllowHtml]
        public string CommentMsg { get; set; }

        public DateTime? CommentDate { get; set; }

        public int? ArticleID { get; set; }

        [StringLength(128)]
        public string UserID { get; set; }

        public string Author { get; set; }
        
        public string AuthorPicture { get; set; }
    }

    public class ApplicationsViewModel
    {
        public int ApplicationID { get; set; }

        [Required]
        [StringLength(50)]
        public string Game { get; set; }

        [Required]
        [StringLength(50)]
        public string CharacterName { get; set; }

        [Required]
        public string Answers { get; set; }

        [StringLength(50)]
        public string Status { get; set; }

        public DateTime ApplicationDate { get; set; }
    }

    public class DefaultViewModel
    {
        public List<TwitchUser> TwitchUsers { get; set; }
    }

    public class UserProfileViewModel
    {
        [StringLength(128)]
        public string UserID { get; set; }

        public string VisitorID { get; set; }

        [Required]
        public string UserName { get; set; }

        [Required]
        public string ProfilePicture { get; set; }

        public string FriendStatus { get; set; }

        public string UserBio { get; set; }
    }

    public class WallPostViewModel
    {
        public int PostID { get; set; }

        [Required]
        [AllowHtml]
        public string PostContent { get; set; }

        [StringLength(128)]
        public string UserID { get; set; }

        [StringLength(128)]
        public string ProfileID { get; set; }

        public DateTime? PostDate { get; set; }

        public string Author { get; set; }

        public string AuthorPicture { get; set; }
    }
}