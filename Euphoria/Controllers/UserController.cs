﻿using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Euphoria.Models;
using System.Configuration;
using System.Data.SqlClient;
using Euphoria.Context;
using Microsoft.AspNet.SignalR;
using Euphoria.Services;
using Microsoft.Web.Mvc;
using System.Web.Http;
using System.Net.Http;
using System.Net;
using System.Net.Http.Formatting;

/// <summary>
/// Summary description for HomeController
/// </summary>
namespace Euphoria.Controllers
{
    public class UserController : Controller
    {
        private ApplicationUserManager _userManager;

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public ActionResult Get(string UserName)
        {
            if (!Request.IsAuthenticated)
            {
                return RedirectToAction("Default", "Home");
            }
            else
            {
                ViewData["UserManager"] = UserManager;
                UserProfileViewModel userProfile = new UserProfileViewModel();
                using (var db = new Connection())
                {
                    userProfile = db.Users.Where(u => u.UserName == UserName).Select(u => new UserProfileViewModel
                    {
                        UserID = u.UserId,
                        UserName = u.UserName,
                        ProfilePicture = u.ProfilePicture,
                        UserBio = u.UserBio
                    }).FirstOrDefault();
                    return View(userProfile);
                }
            }
        }

        [ChildActionOnly]
        public ActionResult GetComments(string UserID)
        {
            List<Context.User> users = CacheService.GetUserList();
            //DB connection
            using (var db = new Connection())
            {
                List<WallPostViewModel> CommentsList = db.ProfilePosts.Where(p => p.ProfileID == UserID).Join(db.Users, p => p.UserID, u => u.UserId, (p, u) => new WallPostViewModel {
                    Author = u.UserName,
                    AuthorPicture = u.ProfilePicture,
                    PostDate = p.PostDate,
                    ProfileID = p.ProfileID,
                    PostContent = p.PostContent,
                    PostID = p.PostID,
                    UserID = p.UserID
                }).OrderByDescending(p => p.PostDate).ToList();
                return PartialView(CommentsList);
            }
        }

        [System.Web.Mvc.HttpPost]
        [System.Web.Mvc.Authorize]
        [ValidateAntiForgeryToken]
        public ActionResult AddComment(WallPostViewModel model)
        {
            if (ModelState.IsValid)
            {
                using (var db = new Connection())
                {
                    var comments = db.Set<ProfilePost>();
                    ProfilePost post = new ProfilePost
                    {
                        ProfileID = model.ProfileID,
                        PostContent = model.PostContent,
                        UserID = model.UserID
                    };
                    comments.Add(post);
                    db.SaveChanges();
                    var context = GlobalHost.ConnectionManager.GetHubContext<Feed>();
                    Euphoria.Context.User user = db.Users.Where(u => u.UserId == model.UserID).FirstOrDefault();
                    Euphoria.Context.User profile = db.Users.Where(u => u.UserId == model.ProfileID).FirstOrDefault();
                    string message = "<a href='/" + user.UserName + "'>" + user.UserName + "</a> posted on <a href='/" + profile.UserName + "#Post" + post.PostID + "'>" + profile.UserName + "'s profile</a>";
                    context.Clients.All.addNewMessageToPage(user.ProfilePicture, message);
                    var feed = db.Set<Activity>();
                    feed.Add(new Activity { ImageSrc = user.ProfilePicture, Message = message });
                    db.SaveChanges();
                    return Redirect("/"+profile.UserName);
                }
            }
            else
            {
                return RedirectToAction("Home", "Home");
            }
        }



        [AjaxOnly]
        [System.Web.Mvc.Authorize]
        public HttpResponseMessage AddFriend([FromBody] FriendRequestAJAX fr)
        {
            try
            {
                using (var db = new Connection())
                {
                    FriendRequest friendRequest = new FriendRequest
                    {
                        UserID1 = fr.UserID1,
                        UserID2 = fr.UserID2,
                        RequestDate = DateTime.UtcNow
                    };
                    db.FriendRequests.Add(friendRequest);
                    db.SaveChanges();
                    HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
                    return result;
                }
            }
            catch (Exception e) {
                HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.ExpectationFailed);
                return result;
            }
        }
    }

    public class FriendRequestAJAX
    {
        public string UserID1 { get; set; }
        public string UserID2 { get; set; }
    }
}