﻿// Decompiled with JetBrains decompiler
// Type: Euphoria.Controllers.HomeController
// Assembly: Euphoria, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 495B1798-C730-4936-A44B-A3E925684694
// Assembly location: E:\Development\Backup\bin\Euphoria.dll

using DiscordWebhook;
using Euphoria.Context;
using Euphoria.Models;
using Euphoria.Services;
using hbehr.recaptcha;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using RestSharp;
using RestSharp.Deserializers;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Xml.Linq;

namespace Euphoria.Controllers
{
    public class HomeController : Controller
    {
        private ApplicationUserManager _userManager;

        public ApplicationUserManager UserManager
        {
            get
            {
                return this._userManager ?? this.HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                this._userManager = value;
            }
        }

        public ActionResult Default()
        {
            if (this.Request.IsAuthenticated)
                return (ActionResult)this.RedirectToAction("Home", "Home");
            List<TwitchUser> twitchList = CacheService.GetTwitchList();
            List<string> onlineUsers = new List<string>();
            List<TwitchUser> twitchUserList = new List<TwitchUser>();
            string str = "?user_id=";
            for (int index = 0; index < twitchList.Count; ++index)
            {
                str += twitchList.ElementAt<TwitchUser>(index).TwitchID;
                if (index < twitchList.Count - 1)
                    str += "&user_id=";
            }
            RestClient restClient = new RestClient("https://api.twitch.tv/helix/");
            RestRequest restRequest = new RestRequest("streams" + str, Method.GET);
            restRequest.AddHeader("Client-ID", "1gwwfmdlmxs9q6uv39q29zjhr0bvfp");
            onlineUsers = new JsonDeserializer().Deserialize<List<ChannelRootObject>>(restClient.Execute((IRestRequest)restRequest)).FirstOrDefault<ChannelRootObject>().data.Select<TwitchChannelResponse, string>((Func<TwitchChannelResponse, string>)(x => x.user_id)).ToList<string>();
            twitchUserList.AddRange((IEnumerable<TwitchUser>)twitchList.Where<TwitchUser>((Func<TwitchUser, bool>)(u => onlineUsers.Contains(u.TwitchID))).ToList<TwitchUser>());
            twitchUserList.AddRange((IEnumerable<TwitchUser>)twitchList.Where<TwitchUser>((Func<TwitchUser, bool>)(u => !onlineUsers.Contains(u.TwitchID))).ToList<TwitchUser>());
            return (ActionResult)this.View((object)new DefaultViewModel()
            {
                TwitchUsers = twitchUserList
            });
        }

        public ActionResult Home()
        {
            if (!this.Request.IsAuthenticated)
                return (ActionResult)this.RedirectToAction("Default", nameof(Home));
            this.ViewData["UserManager"] = (object)this.UserManager;
            return (ActionResult)this.View();
        }

        public ActionResult Apply()
        {
            if (this.Request.IsAuthenticated)
                return (ActionResult)this.RedirectToAction("Home", "Home");
            return (ActionResult)this.View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Apply(ApplicationViewModel model)
        {
            HomeController homeController = this;
            if (!homeController.ModelState.IsValid)
                return (ActionResult)homeController.View();
            string response = homeController.Request["g-recaptcha-response"];
            string content = "```ini\n[" + homeController.Request["Q1"] + "]\n" + model.CharacterName + "\n\n[" + homeController.Request["Q2"] + "]\n" + model.Class + "\n\n[" + homeController.Request["Q3"] + "]\n" + model.Age + "\n\n[" + homeController.Request["Q4"] + "]\n" + model.Location + "\n\n[" + homeController.Request["Q5"] + "]\n" + model.Experience + "\n\n[" + homeController.Request["Q6"] + "]\n" + model.Preference + "\n\n[" + homeController.Request["Q7"] + "]\n" + model.Voice + "\n\n[" + homeController.Request["Q8"] + "]\n" + model.OtherGames + "\n\n[" + homeController.Request["Q9"] + "]\n" + model.PlayerType + "\n\n[" + homeController.Request["Q10"] + "]\n" + model.PrevGuilds + "\n\n[" + homeController.Request["Q11"] + "]\n" + model.LookingFor + "\n\n[" + homeController.Request["Q12"] + "]\n" + model.PrevMMOs + "\n\n[" + homeController.Request["Q13"] + "]\n" + model.Contribute + "\n\n[" + homeController.Request["Q14"] + "]\n" + model.DiscordHandle + "\n\n[" + homeController.Request["Q15"] + "]\n" + model.TwitchHandle + "\n\n[" + homeController.Request["Q16"] + "]\n" + model.YouTubeHandle + "```\n";
            XElement xml = new XElement((XName)"Application", new object[15]
            {
        (object) new XElement((XName) "Answer", new object[2]
        {
          (object) new XAttribute((XName) "Question", (object) homeController.Request["Q2"]),
          (object) model.Class
        }),
        (object) new XElement((XName) "Answer", new object[2]
        {
          (object) new XAttribute((XName) "Question", (object) homeController.Request["Q3"]),
          (object) model.Age
        }),
        (object) new XElement((XName) "Answer", new object[2]
        {
          (object) new XAttribute((XName) "Question", (object) homeController.Request["Q4"]),
          (object) model.Location
        }),
        (object) new XElement((XName) "Answer", new object[2]
        {
          (object) new XAttribute((XName) "Question", (object) homeController.Request["Q5"]),
          (object) model.Experience
        }),
        (object) new XElement((XName) "Answer", new object[2]
        {
          (object) new XAttribute((XName) "Question", (object) homeController.Request["Q6"]),
          (object) model.Preference
        }),
        (object) new XElement((XName) "Answer", new object[2]
        {
          (object) new XAttribute((XName) "Question", (object) homeController.Request["Q7"]),
          (object) model.Voice
        }),
        (object) new XElement((XName) "Answer", new object[2]
        {
          (object) new XAttribute((XName) "Question", (object) homeController.Request["Q8"]),
          (object) model.OtherGames
        }),
        (object) new XElement((XName) "Answer", new object[2]
        {
          (object) new XAttribute((XName) "Question", (object) homeController.Request["Q9"]),
          (object) model.PlayerType
        }),
        (object) new XElement((XName) "Answer", new object[2]
        {
          (object) new XAttribute((XName) "Question", (object) homeController.Request["Q10"]),
          (object) model.PrevGuilds
        }),
        (object) new XElement((XName) "Answer", new object[2]
        {
          (object) new XAttribute((XName) "Question", (object) homeController.Request["Q11"]),
          (object) model.LookingFor
        }),
        (object) new XElement((XName) "Answer", new object[2]
        {
          (object) new XAttribute((XName) "Question", (object) homeController.Request["Q12"]),
          (object) model.PrevMMOs
        }),
        (object) new XElement((XName) "Answer", new object[2]
        {
          (object) new XAttribute((XName) "Question", (object) homeController.Request["Q13"]),
          (object) model.Contribute
        }),
        (object) new XElement((XName) "Answer", new object[2]
        {
          (object) new XAttribute((XName) "Question", (object) homeController.Request["Q14"]),
          (object) model.DiscordHandle
        }),
        (object) new XElement((XName) "Answer", new object[2]
        {
          (object) new XAttribute((XName) "Question", (object) homeController.Request["Q15"]),
          (object) model.TwitchHandle
        }),
        (object) new XElement((XName) "Answer", new object[2]
        {
          (object) new XAttribute((XName) "Question", (object) homeController.Request["Q16"]),
          (object) model.YouTubeHandle
        })
            });
            if (!ReCaptcha.ValidateCaptcha(response, (WebProxy)null))
                return (ActionResult)homeController.RedirectToAction(nameof(Apply), "Home");
            HttpResponseMessage httpResponseMessage = await new Webhook("https://discordapp.com/api/webhooks/479503205261377536/neQ_f845BvQfpwfDVbvfsoEOqIP1d2vBwtEOIIqeXsinURIezm55nFC-NtO9_drhsbhX").Send(content, "Guild Application", (string)null, false, (IEnumerable<Embed>)null);
            using (SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["EuphoriaDB"].ConnectionString))
            {
                SqlCommand sqlCommand1 = new SqlCommand();
                sqlCommand1.CommandType = CommandType.StoredProcedure;
                sqlCommand1.CommandText = nameof(Apply);
                sqlCommand1.Connection = sqlConnection;
                using (SqlCommand sqlCommand2 = sqlCommand1)
                {
                    sqlConnection.Open();
                    sqlCommand2.Parameters.AddWithValue("@CharacterName", (object)model.CharacterName);
                    sqlCommand2.Parameters.AddWithValue("@Game", (object)"MapleStory 2");
                    sqlCommand2.Parameters.AddWithValue("@Answers", (object)xml.ToString());
                    sqlCommand2.ExecuteNonQuery();
                }
            }
            return (ActionResult)homeController.Redirect("~/Confirm");
        }

        [AllowAnonymous]
        public ActionResult Confirm()
        {
            return (ActionResult)this.View();
        }

        [AllowAnonymous]
        public ActionResult Error()
        {
            return (ActionResult)this.View();
        }

        public ActionResult Applications()
        {
            if (this.Request.IsAuthenticated && !this.UserManager.IsInRole<Euphoria.Models.User, string>(this.User.Identity.GetUserId(), "Admin"))
                return (ActionResult)this.RedirectToAction("Home", "Home");
            if (!this.Request.IsAuthenticated)
                return (ActionResult)this.RedirectToAction("Default", "Home");
            this.ViewData["UserManager"] = (object)this.UserManager;
            return (ActionResult)this.View();
        }
    }

    public class ChannelRootObject
    {
        public List<TwitchChannelResponse> data { get; set; }

        public Pagination pagination { get; set; }
    }

    public class Pagination
    {
        public string cursor { get; set; }
    }

    public class TwitchChannelResponse
    {
        public string id { get; set; }

        public string user_id { get; set; }

        public string game_id { get; set; }

        public List<string> community_ids { get; set; }

        public string type { get; set; }

        public string title { get; set; }

        public int viewer_count { get; set; }

        public DateTime started_at { get; set; }

        public string language { get; set; }

        public string thumbnail_url { get; set; }
    }
}
