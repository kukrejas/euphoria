﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Euphoria.Context;
using Euphoria.Models;
using Euphoria;

/// <summary>
/// Summary description for SoulWorkerController
/// </summary>
public class NewsController : Controller
{
    private ApplicationUserManager _userManager;

    public ApplicationUserManager UserManager
    {
        get
        {
            return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
        }
        private set
        {
            _userManager = value;
        }
    }

    [AllowAnonymous]
    public ActionResult Add()
    {
        if (!Request.IsAuthenticated)
        {
            return RedirectToAction("Login", "Account");
        }
        else if (Request.IsAuthenticated && !UserManager.IsInRole(User.Identity.GetUserId(), "Admin"))
        {
            return RedirectToAction("Home", "Home");
        }
        return View();
    }

    [HttpPost]
    [ValidateAntiForgeryToken]
    public ActionResult Add(NewsViewModel model)
    {
        if (ModelState.IsValid)
        {
            string urlpath = Server.MapPath(@"~\Images\Uploads\") + model.FeaturedImage.FileName;

            model.FeaturedImage.SaveAs(urlpath);

            //DB connection
            var connection = ConfigurationManager.ConnectionStrings["EuphoriaDB"];

            //Store article in database
            using (SqlConnection conn = new SqlConnection(connection.ConnectionString))
            {
                using (SqlCommand command = new SqlCommand() { CommandType = System.Data.CommandType.StoredProcedure, CommandText = "AddNews", Connection = conn })
                {
                    conn.Open();
                    command.Parameters.AddWithValue("@Content", model.Content);
                    command.Parameters.AddWithValue("@Author", User.Identity.GetUserName());
                    command.Parameters.AddWithValue("@Category", model.Category);
                    command.Parameters.AddWithValue("@Title", model.Title);
                    command.Parameters.AddWithValue("@Image", "Images/Uploads/" + model.FeaturedImage.FileName);
                    command.ExecuteNonQuery();
                }
            }
        }
        return View();
    }

    public ViewResult View(int id)
    {

        using (var db = new Connection())
        {
            NewsViewModel News = db.News.Where(n => n.ArticleID == id).Select(n => new NewsViewModel
            {
                ArticleID = n.ArticleID,
                Author = n.Author,
                Content = n.Content,
                Published = n.Published,
                Title = n.Title,
                Category = n.Category,
                FeaturedImagePath = n.Image
            }).Single();
            ViewData["UserManager"] = UserManager;
            return View(News);
        }
    }

    [ChildActionOnly]
    public ActionResult Get()
    {
        using (var db = new Connection())
        {
            IList<NewsViewModel> NewsList = db.News.Select(n => new NewsViewModel
            {
                ArticleID = n.ArticleID,
                Author = n.Author,
                Content = n.Content,
                Published = n.Published,
                Title = n.Title,
                Category = n.Category,
                FeaturedImagePath = n.Image
            }).OrderByDescending(d => d.Published).ToList();

            return PartialView(NewsList);
        }
    }

    [ChildActionOnly]
    public ActionResult GetComments(int id)
    {
        //DB connection
        using (var connection = new Procedures())
        {
             IEnumerable<CommentsViewModel> CommentsList = connection.GetNewsComments(id).Select(c => new CommentsViewModel
             {
                 ArticleID = c.ArticleID,
                 Author = c.UserName,
                 AuthorPicture = c.ProfilePicture,
                 CommentDate = c.CommentDate,
                 CommentID = (int) c.CommentID,
                 CommentMsg = c.CommentMsg,
                 ParentID = (int) c.ParentID
             }).ToList();
             return PartialView(CommentsList);
        }
    }

    /*[ChildActionOnly]
    public ActionResult GetComments(int id)
    {
        using (var db = new Connection())
        {
            IList<CommentsViewModel> CommentList = db.Comment.Select(c => new CommentsViewModel
            {
                ArticleID = c.ArticleID,
            }).OrderByDescending(d => d.CommentDate).ToList();
            return PartialView(CommentList);
        }
    }*/

    [HttpPost]
    [System.Web.Mvc.Authorize]
    [ValidateAntiForgeryToken]
    public ActionResult AddComment(CommentsViewModel model)
    {
        if (ModelState.IsValid)
        {
            using (var db = new Connection())
            {
                var comments = db.Set<Comment>();
                Comment com = new Comment
                {
                    ArticleID = model.ArticleID,
                    ParentID = model.ParentID,
                    CommentMsg = model.CommentMsg,
                    UserID = model.UserID
                };
                comments.Add(com);
                db.SaveChanges();
                var context = GlobalHost.ConnectionManager.GetHubContext<Feed>();
                Euphoria.Context.User user = db.Users.Where(u => u.UserId == model.UserID).FirstOrDefault();
                string message = "";
                if (model.ParentID > 0)
                {
                    message = "<a href='/Profile/" + user.UserName + "'>" + user.UserName + "</a> replied to a comment in <a href='/News/View/" + model.ArticleID + "#Comment" + com.CommentID + "'>" + db.News.Where(n => n.ArticleID == model.ArticleID).FirstOrDefault().Title + "</a>";
                }
                else
                {
                    message = "<a href='/Profile/" + user.UserName + "'>" + user.UserName + "</a> commented on <a href='/News/View/" + model.ArticleID + "#Comment" + com.CommentID + "'>" + db.News.Where(n => n.ArticleID == model.ArticleID).FirstOrDefault().Title + "</a>";
                }
                context.Clients.All.addNewMessageToPage(user.ProfilePicture, message);
                var feed = db.Set<Activity>();
                feed.Add(new Activity { ImageSrc = user.ProfilePicture, Message = message });
                db.SaveChanges();
                return RedirectToAction("View", "News", new
                {
                    id = model.ArticleID
                });
            }
        }
        else
        {
            return RedirectToAction("View", "News", new
            {
                id = model.ArticleID
            });
        }
    }


    [AllowAnonymous]
    public ActionResult Confirm()
    {
        return View();
    }
}