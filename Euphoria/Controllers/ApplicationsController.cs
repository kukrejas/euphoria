﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Euphoria.Context;
using Euphoria.Models;

/// <summary>
/// Summary description for SoulWorkerController
/// </summary>
public class ApplicationsController : Controller
{
    private ApplicationUserManager _userManager;

    public ApplicationUserManager UserManager
    {
        get
        {
            return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
        }
        private set
        {
            _userManager = value;
        }
    }

    public ViewResult View(int id)
    {

        using (var db = new Connection())
        {
            ApplicationsViewModel News = db.Applications.Where(a => a.ApplicationID == id).Select(a => new ApplicationsViewModel
            {
                ApplicationID = a.ApplicationID,
                ApplicationDate = a.ApplicationDate,
                CharacterName = a.CharacterName,
                Game = a.Game,
                Answers = a.Answers,
                Status = a.Status
            }).Single();
            ViewData["UserManager"] = UserManager;
            return View(News);
        }
    }

    [ChildActionOnly]
    public ActionResult Get()
    {
        using (var db = new Connection())
        {
            IList<ApplicationsViewModel> Applications = db.Applications.Select(a => new ApplicationsViewModel
            {
                ApplicationID = a.ApplicationID,
                ApplicationDate = a.ApplicationDate,
                CharacterName = a.CharacterName,
                Game = a.Game,
                Answers = a.Answers,
                Status = a.Status
            }).Where(s => s.Status == "Pending").OrderByDescending(d => d.ApplicationDate).Take(10).ToList();
            return PartialView(Applications);
        }
    }

    [ChildActionOnly]
    public ActionResult GetAll()
    {
        using (var db = new Connection())
        {
            IList<ApplicationsViewModel> Applications = db.Applications.Select(a => new ApplicationsViewModel
            {
                ApplicationID = a.ApplicationID,
                ApplicationDate = a.ApplicationDate,
                CharacterName = a.CharacterName,
                Game = a.Game,
                Answers = a.Answers,
                Status = a.Status
            }).OrderByDescending(d => d.ApplicationDate).Take(10).ToList();
            return PartialView(Applications);
        }
    }

    [AllowAnonymous]
    public ActionResult Confirm()
    {
        return View();
    }
}