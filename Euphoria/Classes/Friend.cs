namespace Euphoria.Context
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Friend")]
    public partial class Friend
    {
        public int FriendID { get; set; }

        [Required]
        [StringLength(128)]
        public string UserID1 { get; set; }

        [Required]
        [StringLength(128)]
        public string UserID2 { get; set; }
    }
}
