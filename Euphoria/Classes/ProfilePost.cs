namespace Euphoria.Context
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ProfilePost
    {
        [Key]
        public int PostID { get; set; }

        [Required]
        [StringLength(128)]
        public string ProfileID { get; set; }

        [Required]
        [StringLength(128)]
        public string UserID { get; set; }

        [Required]
        public string PostContent { get; set; }

        public bool ReadStatus { get; set; }

        public DateTime? PostDate { get; set; }
    }
}
