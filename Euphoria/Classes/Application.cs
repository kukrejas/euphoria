namespace Euphoria.Context
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Application
    {
        public int ApplicationID { get; set; }

        [Required]
        [StringLength(50)]
        public string Game { get; set; }

        [Required]
        [StringLength(50)]
        public string CharacterName { get; set; }

        [Required]
        public string Answers { get; set; }

        [StringLength(50)]
        public string Status { get; set; }

        public DateTime ApplicationDate { get; set; }
    }
}
