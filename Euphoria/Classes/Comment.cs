namespace Euphoria.Context
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Comment
    {
        public int CommentID { get; set; }

        public int? ParentID { get; set; }

        public string CommentMsg { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime? CommentDate { get; set; }

        public bool ReadStatus { get; set; }

        public int? ArticleID { get; set; }

        [StringLength(128)]
        public string UserID { get; set; }
    }
}
