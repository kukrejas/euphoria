// Decompiled with JetBrains decompiler
// Type: Euphoria.Services.TwitchResponse
// Assembly: Euphoria, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 495B1798-C730-4936-A44B-A3E925684694
// Assembly location: E:\Development\Backup\bin\Euphoria.dll

namespace Euphoria.Services
{
    public class TwitchResponse
    {
        public string id { get; set; }

        public string login { get; set; }

        public string display_name { get; set; }

        public string type { get; set; }

        public string broadcaster_type { get; set; }

        public string description { get; set; }

        public string profile_image_url { get; set; }

        public string offline_image_url { get; set; }

        public int view_count { get; set; }

        public string email { get; set; }
    }
}
