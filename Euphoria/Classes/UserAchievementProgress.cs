namespace Euphoria.Context
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("UserAchievementProgress")]
    public partial class UserAchievementProgress
    {
        [Key]
        public string UserID { get; set; }

        public int? WallPostCount { get; set; }

        public int? CommentCount { get; set; }

        public int? ProfileViewCount { get; set; }

        public int? FriendCount { get; set; }

        public int? NewsPostViewCount { get; set; }
    }
}
