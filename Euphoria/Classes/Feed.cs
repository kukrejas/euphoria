﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;
using Euphoria.Context;

namespace Euphoria
{
    public class Feed : Hub
    {
        public void Send(string imageSrc, string message)
        {
            // Call the broadcastMessage method to update clients.
            Clients.All.addNewMessageToPage(imageSrc, message);
        }

        public override System.Threading.Tasks.Task OnConnected()
        {
            using(var db = new Connection())
            {
                List<Activity> feedHistory = db.Activity.Select(a => a).OrderByDescending(a => a.EntryID).Take(10).ToList();
                feedHistory.Reverse();
                foreach (var message in feedHistory)
                {
                    Clients.Caller.addNewMessageToPage(message.ImageSrc, message.Message);
                }
            }
            return base.OnConnected();
        }
    }
}