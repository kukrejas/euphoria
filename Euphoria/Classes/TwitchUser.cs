// Decompiled with JetBrains decompiler
// Type: Euphoria.Context.TwitchUser
// Assembly: Euphoria, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 495B1798-C730-4936-A44B-A3E925684694
// Assembly location: E:\Development\Backup\bin\Euphoria.dll

namespace Euphoria.Context
{
    public class TwitchUser
    {
        public string TwitchID { get; set; }

        public string TwitchName { get; set; }
    }
}
