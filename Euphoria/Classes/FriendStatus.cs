using System;

namespace Euphoria.Context
{
    public class FriendStatus
    {
        public int? FriendID { get; set; }

        public int? FriendRequestID { get; set; }

        public string UserID1 { get; set; }

        public string UserID2 { get; set; }

        public DateTime? RequestDate { get; set; }
    }
}
