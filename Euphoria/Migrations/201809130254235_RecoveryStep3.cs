namespace Euphoria.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RecoveryStep3 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Comments", "CommentDate", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Comments", "CommentDate", c => c.DateTime());
        }
    }
}
